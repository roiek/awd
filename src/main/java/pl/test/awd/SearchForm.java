package pl.test.awd;

import pl.test.awd.score.Preferences;


public class SearchForm {
	private Long maxOverallCost = 5000L;
	private Integer limit = 5;
	private Preferences preferences;
	private SearchMode searchMode;
	private String minDays = "1";
	private String maxDays = "10";

	public Long getMaxOverallCost() {

		return maxOverallCost;
	}

	public void setMaxOverallCost(Long maxOverallCost) {

		this.maxOverallCost = maxOverallCost;
	}

	public Integer getLimit() {

		return limit;
	}

	public void setLimit(Integer limit) {

		this.limit = limit;
	}

	public Preferences getPreferences() {

		return preferences;
	}

	public void setPreferences(Preferences preferences) {

		this.preferences = preferences;
	}

	public SearchMode getSearchMode() {

		return searchMode;
	}

	public void setSearchMode(SearchMode searchMode) {

		this.searchMode = searchMode;
	}

	public String getMaxDays() {

		return maxDays;
	}

	public void setMaxDays(String maxDays) {

		this.maxDays = maxDays;
	}

	public String getMinDays() {

		return minDays;
	}

	public void setMinDays(String minDays) {

		this.minDays = minDays;
	}
}
