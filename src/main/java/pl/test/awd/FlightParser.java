package pl.test.awd;

import static java.lang.String.format;
import static java.time.LocalDate.now;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


@Service
public class FlightParser {

	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");


	@Cacheable("flights")
	public List<Flight> fetchFlights(String minDays, String maxDays, int invocationLimit) throws IOException {
		Document document = null;
		System.out.println("Fetching flights");

		try {
			document = getWroclawToWhatever(minDays, maxDays);

		}
		catch (SocketTimeoutException e) {
			System.out.println("SocketTimeoutException, retrying for the " + (invocationLimit+1) + " time.");
			if(invocationLimit <=3) {
				return fetchFlights(minDays, maxDays, invocationLimit+1);
			} else {
				throw new RuntimeException("Restart plx");
			}
		}
//		double scoreForCity = this.scoreCalculate.calculateCityScore("Wroclaw", "JUL", "2018-01-10", "2018-01-14");

		return document.getElementsByClass("result").stream()
			.map(this::toFlight)
			.collect(Collectors.toList());
	}

	private Flight toFlight(Element doc) {

		Flight flight = new Flight();

		flight.setStartCity(doc.getElementsByClass("from").get(0).text().split(" ")[1]);
		try {
			parseTo(doc, flight);
		}
		catch (Exception e) {
			throw new RuntimeException();
		}
		flight.setDuration(Integer.parseInt(doc.getElementsByClass("lengthOfStay").text().replaceAll("\\D+", "")));

		flight.setPrice(Double.parseDouble(doc.getElementsByClass("tp").text().replaceAll("\\D+","")));

		flight.setStartDate(LocalDateTime.of(
			LocalDate.parse(doc.getElementsByClass("date").text().split(" ")[1], DATE_FORMATTER),
			LocalTime.parse(doc.getElementsByClass("from").get(0).text().split(" ")[0], TIME_FORMATTER)));

		flight.setBackDate(LocalDateTime.of(
			LocalDate.parse(doc.getElementsByClass("date").get(1).text().split(" ")[1], DATE_FORMATTER),
			LocalTime.parse(doc.getElementsByClass("to").get(0).text().split(" ")[0], TIME_FORMATTER)));

		flight.setToBuyLink("http://www.azair.cz/" + doc.getElementsByTag("a").get(1).attr("href"));

		return flight;
	}

	private void parseTo(Element doc, Flight flight) throws Exception {

		String cityNameInPolish = doc.getElementsByClass("to").get(0).text().split(" ")[1];
		flight.setPolishCity(cityNameInPolish);

		if(cityNameInPolish.equalsIgnoreCase("Lwów")) {
			flight.setEnglishCity("Lviv");
			return;
		}

		String prepareLink = format(TRANSLATE_STRING, cityNameInPolish);

		HttpHost httpHost = new HttpHost("translate.yandex.net", 80, "http");
		HttpGet get = new HttpGet(prepareLink);
		HttpClient client = HttpClientBuilder.create().build();

		JSONObject object = new JSONObject(EntityUtils.toString(client.execute(httpHost, get).getEntity()));

		flight.setEnglishCity(object.getJSONArray("text").get(0).toString());
	}

	private static final String TRANSLATE_STRING = "/api/v1/tr.json/translate?id=33b4cc63.5a53d65f.17a20bf2-1-0&srv=tr-text&lang=pl-en&reason=auto&text=%s&options=4";

	private Document getWroclawToWhatever(String minDays, String maxDays) throws IOException {

		return Jsoup.connect("http://www.azair.cz/azfin.php"
			+ "?searchtype=flexi"
			+ "&tp=0"
			+ "&isOneway=return"
			+ "&srcAirport=Wroc%C5%82aw+%5BWRO%5D"
			+ "&srcFreeAirport="
			+ "&srcTypedText=Wro"
			+ "&srcFreeTypedText="
			+ "&srcMC="
			+ "&dstAirport=Gdziekolwiek+%5BXXX%5D"
			+ "&anywhere=true"
			+ "&dstap0=LIN"
			+ "&dstap1=BGY"
			+ "&dstFreeAirport="
			+ "&dstTypedText=Gdzie"
			+ "&dstFreeTypedText="
			+ "&dstMC="
			+ "&depmonth=201801"
			+ format("&depdate=%s", now().format(DATE_FORMATTER))
			+ "&aid=0"
			+ "&arrmonth=201810"
			+ format("&arrdate=%s", now().plusMonths(6).format(DATE_FORMATTER))
			+ format("&minDaysStay=%s", minDays)
			+ format("&maxDaysStay=%s", maxDays)
			+ "&dep0=true"
			+ "&dep1=true"
			+ "&dep2=true"
			+ "&dep3=true"
			+ "&dep4=true"
			+ "&dep5=true"
			+ "&dep6=true"
			+ "&arr0=true"
			+ "&arr1=true"
			+ "&arr2=true"
			+ "&arr3=true"
			+ "&arr4=true"
			+ "&arr5=true"
			+ "&arr6=true"
			+ "&samedep=true"
			+ "&samearr=true"
			+ "&minHourStay=0%3A45"
			+ "&maxHourStay=23%3A20"
			+ "&minHourOutbound=0%3A00"
			+ "&maxHourOutbound=24%3A00"
			+ "&minHourInbound=0%3A00"
			+ "&maxHourInbound=24%3A00"
			+ "&autoprice=true"
			+ "&adults=1"
			+ "&children=0"
			+ "&infants=0"
			+ "&maxChng=0"
			+ "&currency=PLN"
			+ "&indexSubmit=Szukaj")
			.cookie("lang", "pl")
			.get();
	}

	@CacheEvict(value ="flights", allEntries = true)
	public void evict() {}
}
