package pl.test.awd.translate;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import com.google.cloud.translate.Translation;
import org.springframework.stereotype.Service;

import java.io.PrintStream;

/**
 * Created by dd on 2018-01-07.
 */
@Service
public class TranslateTextImpl {

    public String translateText(String sourceText, String sourceLang, String targetLang) {

        Translate translate = createTranslateService();
        Translate.TranslateOption srcLang = Translate.TranslateOption.sourceLanguage(sourceLang);
        Translate.TranslateOption tgtLang = Translate.TranslateOption.targetLanguage(targetLang);
        Translate.TranslateOption model = Translate.TranslateOption.model("nmt");

        Translation translation = translate.translate(sourceText, srcLang, tgtLang, model);
        return translation.getTranslatedText();
    }

    private Translate createTranslateService() {
        return TranslateOptions.newBuilder().build().getService();
    }
}
