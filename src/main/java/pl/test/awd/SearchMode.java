package pl.test.awd;

import static pl.test.awd.score.Comparators.BEST_COMPARATOR;
import static pl.test.awd.score.Comparators.CHEAPEST_COMPARATOR;
import static pl.test.awd.score.Comparators.MOST_VALUABLE_COMPARATOR;

import java.util.Comparator;
import java.util.function.Supplier;


public enum SearchMode {

	MOST_VALUABLE("Najopłacalniejszy", () -> MOST_VALUABLE_COMPARATOR),
	CHEAPEST("Najtańszy", () -> CHEAPEST_COMPARATOR),
		BEST("Najatrakcyjniejszy", () -> BEST_COMPARATOR);

	private String label;
	private Supplier<Comparator<Flight>> comparator;

	private SearchMode(String label, Supplier<Comparator<Flight>> comparator) {

		this.label = label;
		this.comparator = comparator;
	}

	public String getLabel() {

		return label;
	}

	public Comparator<Flight> getComparator() {

		return comparator.get();
	}
}
