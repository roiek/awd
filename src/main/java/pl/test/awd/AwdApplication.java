package pl.test.awd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;


@SpringBootApplication
@EnableCaching
public class AwdApplication {

	public static void main(String[] args) {

		SpringApplication.run(AwdApplication.class, args);
	}
}
