package pl.test.awd;

import static pl.test.awd.FlightParser.DATE_FORMATTER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import pl.test.awd.score.PriceInCity;
import pl.test.awd.score.PriceParserImpl;


@Service
public class JourneyCostCalculator {

	@Autowired private PriceParserImpl priceParser;

	@Cacheable("overallCost")
	public Flight fillOverallCost(Flight flight) {

		Double flightPrice = flight.getPrice();
		PriceInCity priceInCity = priceParser.getPricesForCity(flight, flight.getStartDate().format(DATE_FORMATTER), flight.getBackDate().format(DATE_FORMATTER));
		flight.setPriceInCity(priceInCity);
		double pricePerDay = priceInCity.getTicketPrice()*4 + priceInCity.getMealForTwoPeople()*2 + priceInCity.getRoomPrice();
		System.out.println(" * overall per day: " + pricePerDay);
		flight.setOverallPrice(flightPrice + pricePerDay*flight.getDuration());

		return flight;
	}

	@CacheEvict(value = "overallCost", allEntries = true)
	public void evict() {}
}
