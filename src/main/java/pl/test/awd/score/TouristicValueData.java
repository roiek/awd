package pl.test.awd.score;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class TouristicValueData implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired private TouristicValueRepository touristicValueRepository;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

		ObjectMapper objectMapper = new ObjectMapper();

		try {
			List<TouristicValue> myObjects = objectMapper.readValue(new File("test.dat"), new TypeReference<List<TouristicValue>>(){});
			touristicValueRepository.saveAll(myObjects);
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}
}
