package pl.test.awd.score;


import java.io.IOException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

/**
 * Created by dd on 2018-01-07.
 */
@Service
public class QualityOfLifeParserImpl {

    private final static String QYALITY_OF_LIFE_URL = "https://www.numbeo.com/quality-of-life/in/";
    private final static String INDEXES_DELIMITER = "<td style=\"text-align: right\">";
    private final static String INDEXES_SINGLE_PART_DELIMITER = "</td>";
    private final static int NUMBER_VALUE_FROM_PARTS_INEX = 0;

    private final static int QUALITY_OF_LIFE_INDEX = 1;
    private final static int PURCHASING_POWER_INDEX = 2;
    private final static int SAFETY_INDEX = 3;
    private final static int HEALTH_CARE_INDEX = 4;
    private final static int CLIMATE_INDEX = 5;
    private final static int COST_OF_LIVING_INDEX = 6;
    private final static int PROPERTY_PRICE_TI_INCOME_RATIO_INDEX = 7;
    private final static int TRAFFIC_COMMUTE_TIME_INDEX = 8;
    private final static int POLLUTION_INDEX = 9;

	public QualityOfLife getCityIndex(String cityName) {
		QualityOfLife qualityOfLife = new QualityOfLife();
        try {
            setTrustAllCerts();
            Document document = getIndexCityDocument(cityName);
            String[] parts = document.toString().split(INDEXES_DELIMITER);
            qualityOfLife = getScoresFromParts(qualityOfLife, parts);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return qualityOfLife;
    }

    private void setTrustAllCerts() throws Exception
    {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted( java.security.cert.X509Certificate[] certs, String authType ) { }
                    public void checkServerTrusted( java.security.cert.X509Certificate[] certs, String authType ) { }
                }
        };
        try {
            SSLContext sc = SSLContext.getInstance( "SSL" );
            sc.init( null, trustAllCerts, new java.security.SecureRandom() );
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    new HostnameVerifier() {
                        public boolean verify(String urlHostName, SSLSession session) {
                            return true;
                        }
                    });
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    private Document getIndexCityDocument(String cityName) throws IOException {
        return Jsoup.connect(QYALITY_OF_LIFE_URL + cityName).get();
    }

    private QualityOfLife getScoresFromParts(QualityOfLife qualityOfLife, String[] parts){
        qualityOfLife.setQualityOfLifeIndex(getSingleScore(parts[QUALITY_OF_LIFE_INDEX])/10);
        qualityOfLife.setClimateIndex(getSingleScore(parts[CLIMATE_INDEX]));
        qualityOfLife.setCostOfLivingIndex(getSingleScore(parts[COST_OF_LIVING_INDEX]));
        qualityOfLife.setHealthCareIndex(getSingleScore(parts[HEALTH_CARE_INDEX]));
        qualityOfLife.setPollutionIndex(getSingleScore(parts[POLLUTION_INDEX]));
        qualityOfLife.setPurchasingPowerIndex(getSingleScore(parts[PURCHASING_POWER_INDEX]));
        qualityOfLife.setPropertyPriceToIncomeRatio(getSingleScore(parts[PROPERTY_PRICE_TI_INCOME_RATIO_INDEX]));
        qualityOfLife.setSafetyIndex(getSingleScore(parts[SAFETY_INDEX]));
        qualityOfLife.setTrafficCommuteTimeIndex(getSingleScore(parts[TRAFFIC_COMMUTE_TIME_INDEX]));
        return qualityOfLife;
    }

    private double getSingleScore(String phrase){
        String[] parts = phrase.split(INDEXES_SINGLE_PART_DELIMITER);
        try {
            return Double.parseDouble(parts[NUMBER_VALUE_FROM_PARTS_INEX]);
        }
        catch (Exception e) {
            return 0;
        }
    }
}
