package pl.test.awd.score;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class TouristicValue {

	@Id
	private String city;
	private double sightseeing;
	private double nightLife;
	private double children;
	private double sports;
	private double music;

	public double getSightseeing() {

		return sightseeing;
	}

	public void setSightseeing(double sightseeing) {

		this.sightseeing = sightseeing;
	}

	public double getNightLife() {

		return nightLife;
	}

	public void setNightLife(double nightLife) {

		this.nightLife = nightLife;
	}

	public double getChildren() {

		return children;
	}

	public void setChildren(double children) {

		this.children = children;
	}

	public double getSports() {

		return sports;
	}

	public void setSports(double sports) {

		this.sports = sports;
	}

	public double getMusic() {

		return music;
	}

	public void setMusic(double music) {

		this.music = music;
	}

	public String getCity() {

		return city;
	}

	public void setCity(String city) {

		this.city = city;
	}
}
