package pl.test.awd.score;

import javax.persistence.Embeddable;


/**
 * Created by dd on 2018-01-07.
 */
@Embeddable
public class PriceInCity {

    private double mealForTwoPeople;

    private double ticketPrice;

    private double roomPrice;

    public double getMealForTwoPeople() {
        return mealForTwoPeople;
    }

    public void setMealForTwoPeople(double mealForTwoPeople) {
        this.mealForTwoPeople = mealForTwoPeople;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public double getRoomPrice() {
        return roomPrice;
    }

    public void setRoomPrice(double roomPrice) {
        this.roomPrice = roomPrice;
    }
}
