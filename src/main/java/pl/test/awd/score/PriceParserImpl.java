package pl.test.awd.score;

import static java.util.Optional.ofNullable;

import java.io.IOException;
import java.util.Optional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import pl.test.awd.Flight;


/**
 * Created by dd on 2018-01-07.
 */
@Service
public class PriceParserImpl {

    public final static String AIRBNB_URL_FIRST_PART = "https://www.airbnb.pl/s/";
    public final static String COST_OF_LIVE_URL = "https://www.numbeo.com/cost-of-living/in/";
    public final static String AIRBNB_URL_SECOND_PART = "/homes?allow_override%5B%5D=&checkin=";
    public final static String AIRBNB_URL_THIRD_PART = "&checkout=";
    public final static String DOCUMENT_DEMILIMITER = "Średnia cena za dobę w tej okolicy wynosi: ";
    public final static String SINGLE_LINE_DELIMITER = "zł";
    public final static String MEAL_DELIMITER = "<td style=\"text-align: right\" class=\"priceValue \">";
    public final static String MEAL_LAST_DELIMITER = "&nbsp";
    public final static String TICKET_DELIMITER = "<td style=\"text-align: right\" class=\"priceValue \">";
    public final static String TICKET_LAST_DELIMITER = "&nbsp";
    public final static int PRICE_INDEX = 1;
    public final static int PRICE_TICKET_INDEX = 15;


    public PriceInCity getPricesForCity(Flight flight, String checkInDate, String checkOutDate){
        PriceInCity priceInCity = new PriceInCity();
        System.out.println(String.format("Prices for city: %s(%s)", flight.getPolishCity(), flight.getEnglishCity()));
        setRoomPrice(priceInCity, flight.getPolishCity(), checkInDate, checkOutDate);
        setMealAndTicketPrice(priceInCity, flight.getEnglishCity());
        return priceInCity;
    }

    private void setRoomPrice(PriceInCity priceInCity, String cityName, String checkInDate, String checkOutDate) {
        try {
			Document document = getAirBnBDocument(cityName, checkInDate, checkOutDate);
			Optional<Double> price = ofNullable(parseRoomDocument(document.toString()))
				.map(Double::parseDouble);

			System.out.println(" * room: " + price.orElse(0d));

			price.ifPresent(priceInCity::setRoomPrice);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Document getAirBnBDocument(String cityName, String checkInDate, String checkOutDate) throws IOException {
        return Jsoup.connect(AIRBNB_URL_FIRST_PART + cityName
                + AIRBNB_URL_SECOND_PART + checkInDate
                + AIRBNB_URL_THIRD_PART + checkOutDate).get();
    }

    private String parseRoomDocument(String documentContent) {
        String[] parts = documentContent.split(DOCUMENT_DEMILIMITER);
        if (parts != null && parts.length > 3) {
            return parts[PRICE_INDEX].split(SINGLE_LINE_DELIMITER)[0].trim();
        }
        return null;
    }

    private void setMealAndTicketPrice(PriceInCity priceInCity, String cityName){
        try{
            Document document = getCostOfLivingDocument(cityName);

			Optional<Double> mealPrice = ofNullable(parseForMealPrice(document.toString()))
				.map(s -> s.replaceAll(",", ""))
				.map(s -> s.replaceAll(" ", ""))
				.map(Double::parseDouble);
			System.out.println(" * meal: " + mealPrice.orElse(0d));
			mealPrice.ifPresent(priceInCity::setMealForTwoPeople);

			Optional<Double> ticketPrice = ofNullable(parseForTicketPrice(document.toString()))
				.map(Double::parseDouble);
			System.out.println(" * ticket: " + ticketPrice.orElse(0d));
			ticketPrice.ifPresent(priceInCity::setTicketPrice);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private Document getCostOfLivingDocument(String cityName) throws IOException {
        return Jsoup.connect(COST_OF_LIVE_URL + cityName)
			.cookie("defaultCurrency", "PLN")
			.get();
    }

    private String parseForMealPrice(String documentContent){
        String[] parts = documentContent.split(MEAL_DELIMITER);
        if(parts != null && parts.length > 1){
            return parts[PRICE_INDEX].split(MEAL_LAST_DELIMITER)[0].trim();
        }
        return null;
    }

    private String parseForTicketPrice(String documentContent){
        String[] parts = documentContent.split(TICKET_DELIMITER);
        if(parts != null && parts.length > 1){
            return parts[PRICE_TICKET_INDEX].split(TICKET_LAST_DELIMITER)[0].trim();
        }
        return null;
    }

}
