package pl.test.awd.score;

import static java.util.Comparator.comparingDouble;

import java.util.Comparator;

import pl.test.awd.Flight;


public class Comparators {

	public static final Comparator<Flight> MOST_VALUABLE_COMPARATOR = comparingDouble(Flight::getScore).reversed();
	public static final Comparator<Flight> CHEAPEST_COMPARATOR = comparingDouble(Flight::getOverallPrice);
	public static final Comparator<Flight> BEST_COMPARATOR = comparingDouble(Flight::getQuality).reversed();
}
