package pl.test.awd.score;

/**
 * Created by dd on 2018-01-07.
 */
public class QualityOfLife {

    private double qualityOfLifeIndex;

    private double purchasingPowerIndex;

    private double safetyIndex;

    private double healthCareIndex;

    private double climateIndex;

    private double costOfLivingIndex;

    private double propertyPriceToIncomeRatio;

    private double pollutionIndex;

    private double trafficCommuteTimeIndex;

    public QualityOfLife(){
      this.qualityOfLifeIndex = 0;
      this.purchasingPowerIndex = 0;
      this.safetyIndex = 0;
      this.healthCareIndex = 0;
      this.climateIndex = 0;
      this.costOfLivingIndex = 0;
      this.propertyPriceToIncomeRatio = 0;
      this.pollutionIndex = 0;
      this.trafficCommuteTimeIndex = 0;
    }

    public double getQualityOfLifeIndex() {
        return qualityOfLifeIndex;
    }

    public void setQualityOfLifeIndex(double qualityOfLifeIndex) {
        this.qualityOfLifeIndex = qualityOfLifeIndex;
    }

    public double getTrafficCommuteTimeIndex() {
        return trafficCommuteTimeIndex;
    }

    public void setTrafficCommuteTimeIndex(double trafficCommuteTimeIndex) {
        this.trafficCommuteTimeIndex = trafficCommuteTimeIndex;
    }

    public double getPurchasingPowerIndex() {
        return purchasingPowerIndex;
    }

    public void setPurchasingPowerIndex(double purchasingPowerIndex) {
        this.purchasingPowerIndex = purchasingPowerIndex;
    }

    public double getSafetyIndex() {
        return safetyIndex;
    }

    public void setSafetyIndex(double safetyIndex) {
        this.safetyIndex = safetyIndex;
    }

    public double getHealthCareIndex() {
        return healthCareIndex;
    }

    public void setHealthCareIndex(double healthCareIndex) {
        this.healthCareIndex = healthCareIndex;
    }

    public double getClimateIndex() {
        return climateIndex;
    }

    public void setClimateIndex(double climateIndex) {
        this.climateIndex = climateIndex;
    }

    public double getCostOfLivingIndex() {
        return costOfLivingIndex;
    }

    public void setCostOfLivingIndex(double costOfLivingIndex) {
        this.costOfLivingIndex = costOfLivingIndex;
    }

    public double getPropertyPriceToIncomeRatio() {
        return propertyPriceToIncomeRatio;
    }

    public void setPropertyPriceToIncomeRatio(double propertyPriceToIncomeRatio) {
        this.propertyPriceToIncomeRatio = propertyPriceToIncomeRatio;
    }

    public double getPollutionIndex() {
        return pollutionIndex;
    }

    public void setPollutionIndex(double pollutionIndex) {
        this.pollutionIndex = pollutionIndex;
    }
}
