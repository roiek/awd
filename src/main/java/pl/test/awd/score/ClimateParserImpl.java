package pl.test.awd.score;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by dd on 2018-01-07.
 */
@Service
public class ClimateParserImpl {

    public final static String CLIMATE_URL = "https://www.numbeo.com/climate/in/";
    public final static String DOCUMENT_DELIMITER = "\\['";

    public Double getDayAverageTemp(String cityName, String month){
        Document document;
        try {
            document = getClimateDocument(cityName);
            return Double.parseDouble(parseDocument(document.toString(), month));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Document getClimateDocument(String cityName) throws IOException {
        return Jsoup.connect(CLIMATE_URL + cityName).get();
    }

    private String parseDocument(String documentContent, String month){
        String[] parts = documentContent.split(DOCUMENT_DELIMITER);
        if(parts != null && parts.length > 12){
            for(int i = 2; i < 13 ; i++){
                if(parts[i].toLowerCase().contains(month.toLowerCase())){
                    return getTempFromMonthLine(parts[i]);
                }
            }
        }
        return null;
    }

    private String getTempFromMonthLine(String line){
        try {
            return line.trim().split(",")[2].split("\\]")[0];
        }
        catch (Exception e){
            return null;
        }
    }


}
