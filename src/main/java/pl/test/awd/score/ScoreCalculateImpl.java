package pl.test.awd.score;

import static com.google.common.collect.Maps.newHashMap;

import java.util.Comparator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import pl.test.awd.Flight;

/**
 * Created by dd on 2018-01-07.
 */
@Service
public class ScoreCalculateImpl {

    private QualityOfLifeParserImpl qualityOfLifeParser;
    private ClimateParserImpl climateParser;
	private TouristicValueRepository touristicValueRepository;

	@Autowired
	public ScoreCalculateImpl(QualityOfLifeParserImpl qualityOfLifeParser,
		ClimateParserImpl climateParser,
		TouristicValueRepository touristicValueRepository) {

		this.qualityOfLifeParser = qualityOfLifeParser;
		this.climateParser = climateParser;
		this.touristicValueRepository = touristicValueRepository;
	}

	@Cacheable("score")
	public Flight calculateCityScore(Flight flight, Preferences preferences) {


		try {
			String cityName = flight.getEnglishCity();

			TouristicValue touristicValue = touristicValueRepository.getByCity(cityName);
			QualityOfLife qualityOfLife = this.qualityOfLifeParser.getCityIndex(cityName);
			double averageDayTemperature = this.climateParser.getDayAverageTemp(cityName, String.valueOf(flight.getBackDate().getMonthValue()-1));

			System.out.println("disajdklsajdkas: " + averageDayTemperature);

			//            PriceInCity priceInCity = this.roomPriceParser.getPricesForCity(cityName, startDate, endDate);
			double score = calculateAll(qualityOfLife, touristicValue, preferences, averageDayTemperature);

			System.out.println("Score = " + score + " calculated for city " + cityName);
			flight.setTouristicValue(touristicValue);
			flight.setQualityOfLife(qualityOfLife.getQualityOfLifeIndex());
			flight.setQuality(score);

			return flight;
		}
		catch (Exception ignored) {
			return flight;
		}
        }

        private double calculateAll(QualityOfLife qualityOfLife, TouristicValue touristicValue, Preferences preferences, double averageDayTemperature) {

			double score = 0;

			score += touristicValue.getChildren() * preferences.getChildren();
			score += touristicValue.getMusic() * preferences.getMusic();
			score += touristicValue.getNightLife() * preferences.getNightLife();
			score += touristicValue.getSports() * preferences.getSports();
			score += touristicValue.getSightseeing() * preferences.getSightseeing();
			score += qualityOfLife.getQualityOfLifeIndex();
			score -= 2*Math.abs(preferences.getPreferedTemperature() - averageDayTemperature);

			score += scoreFromIndexes(preferences, qualityOfLife);

			return score;
		}

	private double scoreFromIndexes(Preferences preferences, QualityOfLife qualityOfLife) {

		double score = 0;

		Map<String, Double> preferencesMap = newHashMap();
		preferencesMap.put("childen", preferences.getChildren());
		preferencesMap.put("sightseeing", preferences.getSightseeing());
		preferencesMap.put("sports", preferences.getSports());
		preferencesMap.put("nightLife", preferences.getNightLife());
		preferencesMap.put("music", preferences.getMusic());

		return preferencesMap.entrySet().stream()
			.sorted(Comparator.comparingDouble(Map.Entry::getValue))
			.limit(2)
			.mapToDouble(entry -> calculatePreferenceScore(entry, qualityOfLife))
			.sum();
	}

	private double calculatePreferenceScore(Map.Entry<String, Double> entry, QualityOfLife qualityOfLife) {
//		dzieci:
//		-safety
//			-pollution
//		zabytki:
//		-traffic
//			-climate
//
//		sport:
//		-climate
//			-safety
//
//		muzyka:
//		-traffic
//			-healthcare
//
//		zycie nocne:
//		-health
//			-costofliving
		switch(entry.getKey()) {
			case "children":
				return qualityOfLife.getSafetyIndex() + qualityOfLife.getPollutionIndex();
			case "sightseeing":
				return qualityOfLife.getTrafficCommuteTimeIndex() + qualityOfLife.getClimateIndex();
			case "sports":
				return qualityOfLife.getClimateIndex() + qualityOfLife.getSafetyIndex();
			case "nightLife":
				return qualityOfLife.getTrafficCommuteTimeIndex() + qualityOfLife.getCostOfLivingIndex();
			case "music":
				return qualityOfLife.getTrafficCommuteTimeIndex() + qualityOfLife.getHealthCareIndex();
		}

		return 0;
	}

	@CacheEvict(value = "score", allEntries = true)
	public void evict() {}
}
