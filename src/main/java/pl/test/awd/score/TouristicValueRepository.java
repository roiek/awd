package pl.test.awd.score;

import org.springframework.data.repository.CrudRepository;


public interface TouristicValueRepository extends CrudRepository<TouristicValue, String> {

	TouristicValue getByCity(String city);
}
