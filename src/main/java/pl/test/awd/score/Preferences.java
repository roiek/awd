package pl.test.awd.score;

public class Preferences {

	private double sightseeing = 5d;
	private double nightLife = 5d;
	private double children = 5d;
	private double sports = 5d;
	private double music = 5d;
	private double preferedTemperature = 23d;

	public double getSightseeing() {

		return sightseeing;
	}

	public void setSightseeing(double sightseeing) {

		this.sightseeing = sightseeing;
	}

	public double getNightLife() {

		return nightLife;
	}

	public void setNightLife(double nightLife) {

		this.nightLife = nightLife;
	}

	public double getChildren() {

		return children;
	}

	public void setChildren(double children) {

		this.children = children;
	}

	public double getSports() {

		return sports;
	}

	public void setSports(double sports) {

		this.sports = sports;
	}

	public double getMusic() {

		return music;
	}

	public void setMusic(double music) {

		this.music = music;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Preferences that = (Preferences) o;

		if (Double.compare(that.sightseeing, sightseeing) != 0)
			return false;
		if (Double.compare(that.nightLife, nightLife) != 0)
			return false;
		if (Double.compare(that.children, children) != 0)
			return false;
		if (Double.compare(that.sports, sports) != 0)
			return false;
		return Double.compare(that.music, music) == 0;
	}

	@Override
	public int hashCode() {

		int result;
		long temp;
		temp = Double.doubleToLongBits(sightseeing);
		result = (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(nightLife);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(children);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(sports);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(music);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	public double getPreferedTemperature() {

		return preferedTemperature;
	}

	public void setPreferedTemperature(double preferedTemperature) {

		this.preferedTemperature = preferedTemperature;
	}
}
