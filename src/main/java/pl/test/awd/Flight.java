package pl.test.awd;

import static java.time.format.DateTimeFormatter.ofPattern;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

import pl.test.awd.score.PriceInCity;
import pl.test.awd.score.TouristicValue;


public class Flight implements Serializable{

	@Id
	private Long id;
	private String startCity;
	private String polishCity;
	private String englishCity;
	@DateTimeFormat(pattern = "HH:mm dd-MM-yyyy")
	private LocalDateTime startDate;
	@DateTimeFormat(pattern = "HH:mm dd-MM-yyyy")
	private LocalDateTime backDate;
	private Double price;
	private Integer duration;
	private String toBuyLink;
	private double overallPrice;

	private PriceInCity priceInCity;
	private TouristicValue touristicValue;
	private double qualityOfLife;
	private double quality;

	public String getStartCity() {

		return startCity;
	}

	public void setStartCity(String startCity) {

		this.startCity = startCity;
	}

	public String getPolishCity() {

		return polishCity;
	}

	public void setPolishCity(String polishCity) {

		this.polishCity = polishCity;
	}

	public LocalDateTime getStartDate() {

		return startDate;
	}

	public String getStartDateString() {

		return startDate.format(ofPattern("HH:mm"))
			+ "</br>" +
			startDate.format(ofPattern("dd.MM.yyyy"));
	}
	public String getBackDateString() {

		return backDate.format(ofPattern("HH:mm"))
			+ "</br>" +
			backDate.format(ofPattern("dd.MM.yyyy"));
	}

	public void setStartDate(LocalDateTime startDate) {

		this.startDate = startDate;
	}

	public LocalDateTime getBackDate() {

		return backDate;
	}

	public void setBackDate(LocalDateTime backDate) {

		this.backDate = backDate;
	}

	public Double getPrice() {

		return price;
	}

	public void setPrice(Double price) {

		this.price = price;
	}

	public Integer getDuration() {

		return duration;
	}

	public void setDuration(Integer duration) {

		this.duration = duration;
	}

	public String getToBuyLink() {

		return toBuyLink;
	}

	public void setToBuyLink(String toBuyLink) {

		this.toBuyLink = toBuyLink;
	}

	public void setOverallPrice(double overallPrice) {

		this.overallPrice = overallPrice;
	}

	public double getOverallPrice() {

		return overallPrice;
	}

	public String getEnglishCity() {

		return englishCity;
	}

	public void setEnglishCity(String englishCity) {

		this.englishCity = englishCity;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {

		this.id = id;
	}

	public void setPriceInCity(PriceInCity priceInCity) {

		this.priceInCity = priceInCity;
	}

	public PriceInCity getPriceInCity() {

		return priceInCity;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Flight flight = (Flight) o;

		if (polishCity != null ? !polishCity.equals(flight.polishCity) : flight.polishCity != null)
			return false;
		if (englishCity != null ? !englishCity.equals(flight.englishCity) : flight.englishCity != null)
			return false;
		if (startDate != null ? !startDate.equals(flight.startDate) : flight.startDate != null)
			return false;
		return backDate != null ? backDate.equals(flight.backDate) : flight.backDate == null;
	}

	@Override
	public int hashCode() {

		int result = polishCity != null ? polishCity.hashCode() : 0;
		result = 31 * result + (englishCity != null ? englishCity.hashCode() : 0);
		result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
		result = 31 * result + (backDate != null ? backDate.hashCode() : 0);
		return result;
	}

	public double getScore() {

		return 100*quality / overallPrice;
	}

	public TouristicValue getTouristicValue() {

		return touristicValue;
	}

	public void setTouristicValue(TouristicValue touristicValue) {

		this.touristicValue = touristicValue;
	}

	public double getQualityOfLife() {

		return qualityOfLife;
	}

	public void setQualityOfLife(double qualityOfLife) {

		this.qualityOfLife = qualityOfLife;
	}

	public double getQuality() {

		return quality;
	}

	public void setQuality(double quality) {

		this.quality = quality;
	}
}
