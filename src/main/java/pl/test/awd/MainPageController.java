package pl.test.awd;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.test.awd.score.ScoreCalculateImpl;


@Controller
@RequestMapping("/")
public class MainPageController {

	@Autowired private FlightParser flightParser;
	@Autowired private JourneyCostCalculator journeyCostCalculator;
	@Autowired private ScoreCalculateImpl scoreCalculate;

	@GetMapping
	public String getMainPage(Model model) {

		model.addAttribute("searchForm", new SearchForm());
		model.addAttribute("searchModes", SearchMode.values());

		return "main";
	}

	@PostMapping("/search")
	public String getResults(@ModelAttribute SearchForm searchForm, Model model) throws IOException {

		List<Flight> flights = flightParser.fetchFlights(searchForm.getMinDays(), searchForm.getMaxDays(),0).stream()
			.map(journeyCostCalculator::fillOverallCost)
			.map((flight -> scoreCalculate.calculateCityScore(flight, searchForm.getPreferences())))
			.filter(this::hasAllValuesFilled)
			.filter(flight -> Integer.valueOf(searchForm.getMinDays()) <= flight.getDuration() && flight.getDuration() <= Integer.valueOf(searchForm.getMaxDays()))
			.filter(flight -> flight.getOverallPrice() <= searchForm.getMaxOverallCost())
			.limit(searchForm.getLimit())
			.sorted(searchForm.getSearchMode().getComparator())
			.collect(toList());

		model.addAttribute("flights", flights);

		System.out.println("\n\nFinished\n\n");

		return "results";
	}

	private boolean hasAllValuesFilled(Flight flight) {

		return flight.getPrice() != null &&
			flight.getPriceInCity().getRoomPrice() != 0d &&
			flight.getPriceInCity().getMealForTwoPeople() != 0d &&
			flight.getPriceInCity().getTicketPrice() != 0d &&
			flight.getQuality() != 0d;
	}

	@GetMapping("/clearCache")
	public String removeAll() {

		flightParser.evict();
		journeyCostCalculator.evict();
		scoreCalculate.evict();

		return "redirect:/";
	}
}
