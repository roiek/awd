package pl.test.awd;

import static com.google.common.collect.Lists.newArrayList;

import java.io.File;
import java.text.Normalizer;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.test.awd.score.TouristicValue;


public class JourneyCostCalculatorTest {

	private static final List<String> cities = newArrayList("Gdańsk", "Lviv", "London", "Paris", "Amsterdam", "Frankfurt", "Istanbul", "Madrid", "Barcelona", "Munich", "Rome", "Moscow", "Copenhagen", "Dublin", "Zürich", "Palma de Mallorca", "Oslo", "Manchester", "Stockholm", "Düsseldorf", "Vienna", "Lisbon", "Brussels", "Berlin", "Athens", "Milan", "Helsinki", "Málaga", "Geneva", "Hamburg", "Saint Petersburg", "Prague", "Warsaw", "Nice", "Edinburgh", "Alicante", "Las Palmas de Gran Canaria", "Cologne / Bonn", "Birmingham", "Budapest", "Bergamo / Milan", "Bucharest", "Stuttgart", "Santa Cruz de Tenerife", "Venice", "Lyon", "Porto", "Glasgow", "Kiev", "Marseille", "Toulouse", "Catania", "Bologna", "Faro", "Bristol", "Ibiza", "Basel / Mulhouse / Freiburg im Breisgau", "Charleroi", "Reykjavík", "Naples", "Heraklion", "Lanzarote", "Larnaca", "Gothenburg", "Bergen", "Valencia", "Bordeaux", "Sofia", "Thessaloniki", "Puerto del Rosario", "Hanover", "Riga", "Palermo", "Sochi", "Simferopol", "Belfast", "Malta", "Pisa", "Kraków", "Rhodes", "Belgrade", "Newcastle upon Tyne", "Nantes", "Liverpool", "Eindhoven", "Derby / Leicester / Nottingham", "Seville", "Bilbao");

	private static final Random random = new Random();

	@Test
	public void should() throws Exception {

		// given
		List<TouristicValue> touristicValue = cities.stream()
			.map(city -> Normalizer.normalize(city, Normalizer.Form.NFKD))
			.map(StringUtils::stripAccents)
			.map(this::getSomeRandomness)
			.collect(Collectors.toList());

		System.out.println("wqe");

		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(new File("test.dat"), touristicValue);
		// when

		// then

	}

	private TouristicValue getSomeRandomness(String cityName) {

		TouristicValue touristicValue = new TouristicValue();

		touristicValue.setCity(cityName);
		touristicValue.setChildren(randomFrom1To10());
		touristicValue.setMusic(randomFrom1To10());
		touristicValue.setNightLife(randomFrom1To10());
		touristicValue.setSports(randomFrom1To10());
		touristicValue.setSightseeing(randomFrom1To10());

		return touristicValue;
	}

	private int randomFrom1To10() {

		return random.nextInt(10) +1;
	}

}